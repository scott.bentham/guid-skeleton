package uk.gov.dwp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import uk.gov.dwp.rest.ScottishGovernmentClient;

/**
 * SpringBoot application for the application.
 */
@SpringBootApplication
@SuppressWarnings("PMD.UseUtilityClass")
public class Application {


  private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);
  /**
   * Main entry method for the application.
   *
   * @param args Command line arguments to the application
   */
  public static void main(final String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
