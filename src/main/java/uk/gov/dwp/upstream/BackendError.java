package uk.gov.dwp.upstream;

import lombok.Getter;

/**
 * enum to store error constants used to distinguish various Backend Exceptions.
 */
@Getter
public enum BackendError {

  UNAUTHORISED("Unauthorized [401]"),
  SERVICE_TIMEOUT("Read timed out"),
  GATEWAY_ERROR("Gateway error"),
  SOAP_FAULT("Soap Fault"),
  MARSHALLING_ERROR("Marshalling Error"),
  UNMARSHALLING_ERROR("Unmarshalling Error"),
  UNKNOWN_ERROR("Unknown error");

  /**
   * Error Type variable value.
   */
  private final String description;

  /**
   * Constructor which initialises the error type variable value.
   */
  BackendError(final String description) {
    this.description = description;
  }
}
