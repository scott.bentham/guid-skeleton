package uk.gov.dwp.rest;

//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;





/**
 * Rest controller for the Scottish government api013 service.
 */
@SuppressFBWarnings({"SPRING_ENDPOINT", "NP_LOAD_OF_KNOWN_NULL_VALUE"})
@SuppressWarnings({"PMD.BeanMembersShouldSerialize", "PMD.ImmutableField"})
@RestController
@RequestMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
public class ScottishGovernmentClient {

  private static final Logger LOGGER = LoggerFactory.getLogger(ScottishGovernmentClient.class);

  @PostMapping(path = "/guid-mapper"
//      value = "/content",
//      produces = MediaType.APPLICATION_XML_VALUE
  )
  public final ResponseEntity<String> buildApi013Response(
      @RequestParam Map<String,String> params,
      @RequestHeader MultiValueMap<String, String> headers,
      @RequestBody String requestBody
      ) {
      headers.forEach((key, value) -> {
        LOGGER.info(String.format(
            "Header '%s' = %s", key, value.stream().collect(Collectors.joining("|"))));
      });


    ResponseEntity<String> responseEntity;

    responseEntity = new ResponseEntity<>(
        requestBody,
        headers,
        HttpStatus.OK);

    return responseEntity;
  }




}
