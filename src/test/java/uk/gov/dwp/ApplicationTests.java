package uk.gov.dwp;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

@SpringBootTest
public class ApplicationTests {

  @Autowired
  private ApplicationContext context;

  @Test
  public void contextLoads() {
    assertNotNull(context);
  }

  @Test
  public void testMain() {
    Application.main(new String[]{});
  }
}
