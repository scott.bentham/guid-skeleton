Feature:Validation Scenarios
  As a user of the service I should be able to  validate the fields that are sent in the request and capture the error messages.


#  Scenario Outline:<id> NINO validation
#    Given a valid request from "<consumer>" with nino "<nino>" and dob "1990-09-27" and surname "Lewis"
#    When the request is sent
#    Then a status <status> code is returned
#    And the response result element has value "<result>"
#    And a checkEligibilityResponse faultDetail containing "<faultDetail>" is returned
#    And a checkEligibilityResponse exceptionName containing "<exceptionNameValue>" is returned
#    And a checkEligibilityResponse exceptionDetail containing "<exceptionDetailValue>" is returned
#    Examples:DFE
#      | id | description      | consumer | nino        | status | result  | faultDetail                                                                                                                                                                                                                                   | <null>             | <null>                                               |
#      | 1  | valid nino       | dfe      | HP001718     | 200    | success | <null>
#
#
#| Incomplete Request | Error 4 : Mandatory data not supplied : Missing NINO |



  Scenario Outline:<id> NINO validation232
    Given a valid request from "<consumer>" with nino "<nino>" and dob "1990-09-27" and surname "Lewis"
    When the rest request is sent
    Then a status <status> code is returned
    Examples:DFE
      | id | description      | consumer | nino        | status | result  | faultDetail                                                                                                                                                                                                                                    | <null>             | <null>                                               |
      | 1  | valid nino       | dfe      | HP001718     | 200    | success | <null>                                                                                                                                                                                                                                 | <null>             | <null>                                               |
#      | 2  | empty nino       | dfe      |             | 200    | fail    | <null>