import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.DataProvider;

@CucumberOptions(
    features = {"src/integration-test/resources/features"},
    plugin = {"pretty", "json:target/jsonReports/cucumber.json"},
    glue = {"definitions"}
)
public class CucumberIT extends AbstractTestNGCucumberTests {

  @DataProvider(parallel = false)
  @Override
  public Object[][] scenarios() {
    return super.scenarios();
  }

}
