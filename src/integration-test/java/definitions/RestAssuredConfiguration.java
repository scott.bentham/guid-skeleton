package definitions;

import io.restassured.RestAssured;

public class RestAssuredConfiguration {
  /*
    Default service port
   */
  private static final String defaultPort = "8043";

  /*
    Default service host
   */
  private static final String defaultHostname = "localhost";

  /*
    Default service protocol
   */
  private static final String defaultProtocol = "http";

  /*
    Default base path
   */
  private static final String defaultBasePath = "/cxf/foil/webservices/passportedbenefits";

  /*
    Create base uri from protocol, host & port
   */
  private static String buildUri(final String protocol, final String host, final String port) {
    return String.format("%s://%s:%s", protocol, host, port);
  }

  /*
    Get the protocol from configuration or default value
   */
  private static String GetProtocol() {
    final String configuredProtocol = System.getProperty("service.protocol");

    return configuredProtocol != null ? configuredProtocol : defaultProtocol;
  }

  /*
    Get the hostname from configuration or default value
   */
  private static String GetHostname() {
    final String configuredHostname = System.getProperty("service.hostname");

    return configuredHostname != null ? configuredHostname : defaultHostname;
  }

  /*
    Get the port from configuration or default value
   */
  private static String GetPort() {
    final String configuredPort = System.getProperty("service.port");

    return configuredPort != null ? configuredPort : defaultPort;
  }

  /*
    Configure RestAssured with uri & base path
   */
  public static void Configure() {
    final String basePath = System.getProperty("service.basepath");

    RestAssured.baseURI = buildUri(GetProtocol(), GetHostname(), GetPort());
    RestAssured.basePath = basePath != null ? basePath : defaultBasePath;
    RestAssured.useRelaxedHTTPSValidation();
  }
}
