package definitions;

import static io.restassured.RestAssured.basePath;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.internal.util.IOUtils;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import org.apache.commons.lang3.time.DateUtils;
import org.hamcrest.Matchers;
import org.testng.Assert;

public class Definitions {

  class Person {
    public String name;
    public String surname;
    public int age = 0;
    public String ninoGuid;
  }

  class GenericSampleObject {
    public Person[] people;
    public String relationshipType = "";
    public String relationshipGuid;
  }

  private GenericSampleObject getSampleData(){
    Person personA = new Person();
    personA.name = "scott";
    personA.surname = "bentham";
    personA.age = 18;
    personA.ninoGuid = "12345-12345";


    Person personB = new Person();
    personB.name = "ketih";
    personB.surname = "beandresntham";
    personB.age = 25;
    personB.ninoGuid = "67890-67890";

    GenericSampleObject sam = new GenericSampleObject();
    sam.relationshipType = "mates";
    sam.relationshipGuid = "13579-24680";
    sam.people = new Person[2];
    sam.people[0] = personA;
    sam.people[1] = personB;

    return sam;
  }

  private RequestSpecification request = given();
  private Response response;
  private String path = "http://passported-benefits:8043/cxf/foil/webservices/passportedbenefits";
  private Map systems = new HashMap<String, Integer>();
  private String requestBody;



  public void setupRest() {
    basePath = "http://passported-benefits:8043/cxf/foil/rest/passportedbenefits" ;
    RestAssured.basePath = "/cxf/foil/rest/passportedbenefits" ;
    path = baseURI + basePath;
  }



  @Before
  public void Setup() {
    RestAssuredConfiguration.Configure();
    path = baseURI + basePath;

    setupRest();;


    systems.put("dfe", 606);
    systems.put("bt", 605);
    systems.put("moj", 604);
    systems.put("invalid", 99999);
  }


  @Given("a valid request from {string} with nino {string} and dob {string} and surname {string}")
  public void aValidRequestWithGivenValues(String consumer,String nino, String dob,String surname) {
    aValidRequestWithAllGivenValuesMaster(consumer, nino, dob, surname, "", "", "", "");
  }

  @Given("a default valid request from {string}")
  public void aDefaultValidRequest(String consumer) {
    aValidRequestWithGivenValues(consumer, "AA123456C", "1995-02-05", "KAN");
  }

  @Given("^a valid request from \"([^\"]*)\" with dateFrom \"([-]?[0-9]*)\" and dateTo \"([-]?[0-9]*)\" and Threshold \"([^\"]*)\"$")
  public void aValidRequestWithAllGivenValuesNew(String consumer, String daysFromTodayDateFrom, String daysFromTodayDateTo, String threshold) {
    final String dateFrom = getDateFromDaysFromDate(daysFromTodayDateFrom);
    final String dateTo = getDateFromDaysFromDate(daysFromTodayDateTo);
    aValidRequestWithAllGivenValuesMaster(consumer, "AA123456C", "1995-02-05", "KAN", dateFrom, dateTo, threshold, "");
  }

  @Given("^a valid request from \"([^\"]*)\" with dateFrom \"(([0-9]{4}[-][0-9]{2}[-][0-9]{2})?)\" and dateTo \"(([0-9]{4}[-][0-9]{2}[-][0-9]{2})?)\" and Threshold \"([^\"]*)\"$")
  public void aValidRequestWithAllGivenValuesNew2(String consumer, String dateFrom, String dateTo, String threshold) {
    aValidRequestWithAllGivenValuesMaster(consumer, "AA123456C", "1995-02-05", "KAN", dateFrom, dateTo, threshold, "");
  }

  @Given("a valid request from {string} with nino {string} dob {string} surname {string} dateFrom {string} dateTo {string} and threshold {string}")
  public void aValidRequestWithAllGivenValuesNew(String consumer, String nino, String dob, String surname, String dateFrom, String dateTo, String threshold) {
    aValidRequestWithAllGivenValuesMaster(consumer, nino, dob, surname, dateFrom, dateTo, threshold,"");
  }

  @Given("a valid request from consumer {string} with nino {string}, dob {string}, surname {string} and threshold {string}")
  public void aValidRequestWithAllGivenValuesNew(String consumer, String nino, String dob, String surname, String threshold) {
    aValidRequestWithAllGivenValuesMaster(consumer, nino, dob, surname, "previousMonthDateFrom", "currentDateTo", threshold,"");
  }

  @Given("a valid request with transactionId {string}")
  public void aValidRequestWithTransactionId(String transactionId) {
    aValidRequestWithAllGivenValuesMaster("dfe", "HP001718C", "1950-04-01", "Tucker", "previousMonthDateFrom", "currentDateTo", "3564.00", transactionId);
  }

  private void aValidRequestWithAllGivenValuesMaster(String consumer, String nino, String dob, String surname, String dateFrom, String dateTo, String threshold, String transactionId) {
    String datePattern = "yyyy-MM-dd";
    if(transactionId.equals("")) {
      transactionId = getRandomGeneratedTransactionId();
    }

    if (dateFrom.equals("previousMonthDateFrom")) {
      dateFrom = new SimpleDateFormat(datePattern).format(DateUtils.addMonths(new Date(),-1));
    }

    if (dateTo.equals("currentDateTo")) {
      dateTo = new SimpleDateFormat(datePattern).format(new Date());
    }

    requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"http://dwp.gov.uk/benefitCheck/1.0.0\">\n" +
        "   <soapenv:Header/>\n" +
        "   <soapenv:Body>\n" +
        "      <ns:CheckEligibilityRequest>\n" +
        "         <ns:foilHeader>\n" +
        "            <ns:organisationUnit>?</ns:organisationUnit>\n" +
        "            <ns:systemID>"+systems.get(consumer)+"</ns:systemID>\n" +
        "            <ns:subSystemID>2</ns:subSystemID>\n" +
        "            <ns:departmentID>2</ns:departmentID>\n" +
        "            <ns:locationAddress>2</ns:locationAddress>\n" +
        "            <ns:officeID>2</ns:officeID>\n" +
        "            <ns:transactionID>" + transactionId + "</ns:transactionID>\n" +
        "            <ns:userID>2</ns:userID>\n" +
        "            <ns:userName>2</ns:userName>\n" +
        "         </ns:foilHeader>\n" +
        "         <ns:NINO>"+nino+"</ns:NINO>\n" +
        "         <ns:dateOfBirth>"+dob+"</ns:dateOfBirth>\n" +
        "         <ns:surname>"+surname+"</ns:surname>\n" +
        (dateFrom == null || dateFrom.equals("") ? "" : "<ns:thresholdDateFrom>" + dateFrom + "</ns:thresholdDateFrom>\n") +
        (dateTo == null || dateTo.equals("") ? "" : "<ns:thresholdDateTo>" + dateTo + "</ns:thresholdDateTo>\n") +
        (threshold == null || threshold.equals("") ? "" : "<ns:thresholdAmount>" + threshold + "</ns:thresholdAmount>\n") +
        "       </ns:CheckEligibilityRequest>\n" +
        "   </soapenv:Body>\n" +
        "</soapenv:Envelope>";
  }

  private String getRandomGeneratedTransactionId() {
    final Random random = new Random();
    final int randomLengthOfTransactionId = 3 + random.nextInt(18); //between 3 to 20 in length
    final String randomGeneratedString = randomAlphaNumeric(randomLengthOfTransactionId);
    return randomGeneratedString;
  }

  private static final String ALPHA_NUMERIC_STRING = "ACEFGHJKLMNPQRUVWXYabcdefhijkprstuvwx0123456789";
  public static String randomAlphaNumeric(int count) {
    StringBuilder builder = new StringBuilder();
    while (count-- != 0) {
      int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
      builder.append(ALPHA_NUMERIC_STRING.charAt(character));
    }
    return builder.toString();
  }

  @When("the request is sent")
  public void theRequestIsSent() {
    request.body(requestBody);
    request.header("Content-Type","text/xml");
    request.header("trackingId", UUID.randomUUID().toString());
    this.response = request.relaxedHTTPSValidation().post(path);
  }


  @When("the rest request is sent")
  public void therestRequestIsSent() {
    int i = 3;
    request.body(getSampleData());
    request.header("Content-Type","application/json");
    request.header("trackingId", UUID.randomUUID().toString());
    this.response = request.relaxedHTTPSValidation().post(path);
  }

  @When("^the request without trackingId is sent$")
  public void theRequestWithoutTrackingIdIsSent() {
    request.body(requestBody);
    request.header("Content-Type","text/xml");
    this.response = request.relaxedHTTPSValidation().post(path);
  }

  @When("the request is sent with trackingId {string}")
  public void theRequestIsSentWithTrackingId(String trackingId) {
    request.body(requestBody)
        .header("Content-Type","text/xml")
        .header("trackingId", trackingId);
    this.response = request.relaxedHTTPSValidation().post(path);
  }

  @Then("the {string} element is returned as null")
  public void theFieldElementIsReturnedAsNull(String field) {
    this.response.then().body(field, equalTo(null));
  }

  @And("a status {int} code is returned")
  public void aStatusCodeIsReturned(Integer statusCode) {
    System.out.println("Response is " + this.response.body().prettyPrint());
    this.response.then().statusCode(statusCode);
    int i = 4;
  }

  @And("an error message is shown")
  public void anErrorMessageIsShown() {
    this.response.then().body("responseHeader.result.category", equalTo("Error"));
  }

  @And("a exception error with the message {string} is shown")
  public void aSystemErrorWithTheMessageIsShown(String errorMessage) {
    this.response.then().body(Matchers.containsString(errorMessage));
  }

  @Then("^the response should match the json (.+)$")
  public void theResponseShouldMatchTheJson(String jsonFilename) throws Exception {
    String user_dir = "src/integration-test/resources/expected-responses/";
    InputStream is = new FileInputStream(user_dir + jsonFilename);
    String expectedResponse = new String(IOUtils.toByteArray(is));
    String actualResponse = response.body().asString();
    Assert.assertEquals(actualResponse,expectedResponse);
  }

  @Then("the CheckEligibilityResponse result attribute has value {string}")
  public void assertCheckEligibilityResponseResultHasGivenValue(final String value) {
    assertTheValueAtGivenXPath_hasGivenValue("Envelope.Body.CheckEligibilityResponse.@result", value,
        AssertType.EQUALS);
  }

  @And("^a response element of \"([^\"]*)\" is returned$")
  public void aResponseElementOfIsReturned(String arg0) throws Throwable {
    this.response.then().body(Matchers.containsString("<response>"+arg0+"</response>"));
  }

  @And("^a faultCode \"([^\"]*)\" is returned$")
  public void aFaultCodeIsReturned(String arg0)  {
    this.response.then().body(Matchers.containsString("<faultCode>"+arg0+"</faultCode>"));
  }

  @And("^a faultcode \"([^\"]*)\" is returned$")
  public void aFaultCodeIsReturnedLowerCase(String arg0) {
    this.response.then().body(Matchers.containsString("<faultcode>"+arg0+"</faultcode>"));
  }

  @And("^a fault string \"([^\"]*)\" is returned$")
  public void aFaultStringIsReturned(String arg0)  {
    this.response.then().body(Matchers.containsString("<faultstring>"+arg0));
  }

  @And("^a fault detail \"([^\"]*)\" is returned$")
  public void aFaultDetailIsReturned(String arg0)  {
    this.response.then().body(Matchers.containsString("<faultDetail>"+arg0+"</faultDetail>"));
  }

  @And("the element {string} value changed to {string}")
  public void setRequestElementValue(String element, String newValue) {
    final String replacement;
    final String regexToReplace = "(?s)<"+element+"[^>]*>.*?</"+element+">";
    if(newValue.trim().equals("<null>")) {
      replacement = "";
    }
    else {
      replacement = "<"+element+">"+newValue+"</"+element+">";
    }
    this.requestBody = this.requestBody.replaceFirst(regexToReplace, replacement);
  }

  @And("^a checkEligibilityResponse exceptionDetail containing \"([^\"]*)\" is returned$")
  public void aCheckEligibilityResponseExceptionDetailsContainingIsReturned(String value) {
    if (value.trim().equals("<null>")) {
      assertTrue(true);
    } else {
      assertTheValueAtGivenXPath_hasGivenValue(
          "Envelope.Body.CheckEligibilityResponse.Exception.exceptionDetails", value, AssertType.CONTAINS);
    }
  }

  @And("a checkEligibilityResponse exceptionName containing {string} is returned")
  public void aCheckEligibilityResponseExceptionNameContainingIsReturned(String value) {
    if (value.trim().equals("<null>")) {
      assertTrue(true);
    } else {
      assertTheValueAtGivenXPath_hasGivenValue(
              "Envelope.Body.CheckEligibilityResponse.Exception.exceptionName", value, AssertType.CONTAINS);
    }
  }

  @And("^a faultString containing \"([^\"]*)\" is returned$")
  public void aFaultStringContainingValueIsReturned(String value) {
    if (value.trim().equals("<null>")) {
      assertTrue(true);
    } else {
      assertTheValueAtGivenXPath_hasGivenValue("Envelope.Body.Fault.faultstring", value, AssertType.CONTAINS);
    }
  }

  @Then("the response result element has value {string}")
  public void theResponseResultElementHasGivenValue(final String value) {
      assertTheValueAtGivenXPath_hasGivenValue(
              "Envelope.Body.CheckEligibilityResponse.@result", value, AssertType.EQUALS);
  }

  @And("^a fault sub code \"([^\"]*)\" is returned$")
  public void aFaultSubCodeIsReturned(String arg0) {
    this.response.then().body(Matchers.containsString("<faultSubCode>"+arg0+"</faultSubCode>"));
  }

  private void assertTheValueAtGivenXPath_hasGivenValue(String xPath, String value,
      AssertType assertType) {
    if (response.statusCode() == 500 && xPath.contains("Fault.faultDetail")) {
      xPath = "Envelope.Body.Fault.faultstring";
    }
    if (value.trim().equals("<null>")) {
      assertTrue(true);
    } else {
      String actualValue = response.xmlPath().get(xPath);
      if (assertType == assertType.EQUALS) {
        assertThat(actualValue, Matchers.equalTo(value));
      } else if (assertType == assertType.CONTAINS) {
        assertThat(actualValue, Matchers.containsString(value));
      }
    }
  }

  @And("a checkEligibilityResponse faultDetail containing {string} is returned")
  public void aCheckEligibilityResponseFaultDetailContainingIsReturned(String value) {
    if (value.trim().equals("<null>")) {
      assertTrue(true);
    }
    else {
      assertTheValueAtGivenXPath_hasGivenValue(
              "Envelope.Body.CheckEligibilityResponse.Fault.faultDetail", value, AssertType.CONTAINS);
    }
  }

  @And("a checkEligibilityResponse faultString containing {string} is returned")
  public void aCheckEligibilityResponseFaultStringContainingIsReturned(String value) {
      if (value.trim().equals("<null>")) {
          assertTrue(true);
      } else {
          assertTheValueAtGivenXPath_hasGivenValue("Envelope.Body.Fault.faultstring", value, AssertType.CONTAINS);
      }
  }

  private enum AssertType {
    CONTAINS(1),
    EQUALS(2);

    int code;

    AssertType(int code) {
      this.code = code;
    }
  }

  private String getDateFromDaysFromDate(final String daysFromDate){
    String date;
    if(daysFromDate.equals("")){
      date = daysFromDate;
    }
    else {
      final int daysFromTodayDateToInt = Integer.parseInt(daysFromDate);
      date = getCurrentDateWithDaysAdjusted(daysFromTodayDateToInt);
    }
    return date;
  }

  private String getCurrentDateWithDaysAdjusted(final int numberOfDaysToAdd) {
    final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    final Date currentDate = new Date();
    final Calendar c = Calendar.getInstance();
    c.setTime(currentDate);
    c.add(Calendar.DATE, numberOfDaysToAdd);
    final Date currentDateAdjusted = c.getTime();
    final String adjustedDate = dateFormat.format(currentDateAdjusted);
    return adjustedDate;
  }
}
