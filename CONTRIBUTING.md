## How to Contribute

This is a quick and simple guide on how work should be contributed to this project.

### Basic Steps

1. Clone, or fork, the project
1. Create your feature branch (`git checkout -b feature/foobar`)
1. Commit your changes (`git commit -m 'feat(*): add wibble to the sprockets'`)
1. Push to the branch (`git push origin feature/foobar`)
1. Create a new merge request when work is complete

### Branch Strategy

The project follows Git Flow.

### Merging a branch

When you have completed your changes, open up a merge request to merge your branch into it's parent branch (e.g. **develop**, or an **epic/** branch).

Be prepared to receive feedback on your work and discuss any issues that arise during the review.
